{- |
Module      :  <robo_architect>
Description :  <Solver for an architect problem>
Copyright   :  <PW, WEiTI>
License     :  <Academic Free License>

Maintainer  :  <B.Piekarski@stud.elka.pw.edu.pl, R.Pietrzak@stud.elka.pw.edu.pl>
Stability   :  stable
-}
-- for IO
import System.Environment
import System.Exit
import System.IO

-- for easier list management
import Data.List

-- for function bad input handling
import Data.Maybe

-- for debuging
import Debug.Trace
mydebug = flip trace

--dispatch :: [(String, [String] -> IO ())]
--dispatch =    [ ("-s", save)
--          , ("-h", help)
--          , ("-v", verbose)
--          ]   
data Tile       =   Empty   
                |   Place   Position Bool                   --  Bool- can be a tank
                |   House   Position Tile                   --  Tile- should be a Tank or Empty (if tank is not yet found for this house)
                |   Tank    Position Tile deriving (Show)   --  tile- should be a House or Empty

type Position       = (Int, Int)                
------------------------------
-- helper types --------------
------------------------------
type Map        = ([Int], [Int], Matrix)
--type Grid       = Matrix 
type Matrix     = [Row]
type Row        = [Tile]
type Column     = [Tile]
--type Value      = Tile 

------------------------------
-- getters  ------------------
------------------------------
getRowsValues :: Map -> [Int]
getRowsValues (r,c,g)       = r

getColumnsValues :: Map -> [Int]
getColumnsValues (r,c,g)    = c

getGrid :: Map -> Matrix
getGrid (r,c,g)             = g

-- no size checking (checked earlier)
getRow :: Map -> Int -> Row 
getRow (r,c,g) no           = g !! no

getColumn :: Map -> Int -> Column 
getColumn (r,c,g) no        = gt !! no
    where
        gt = transpose g 

------------------------------
-- input string handling -----
------------------------------
getNums :: String -> [Int]
getNums []  =   []
getNums a   =   let stripped = a `intersect` ['0','1','2','3','4','5','6','7','8','9',',']
                in
                    map (read::String->Int) $ words $ map repl stripped
    where 
        repl ',' = ' '
        repl c = c

getTuples :: String -> [(Int,Int)]
getTuples []    =   []
getTuples a     =   let stripped = a `intersect` ['0','1','2','3','4','5','6','7','8','9',',','(',')']
                        tuplesStr = words $ map repl stripped
                        tuplesStrStripped = map (map repl') tuplesStr 
                        tuplesStrStripped' = map words tuplesStrStripped
                        tuplesInt = map ( map ( read::String->Int) ) tuplesStrStripped'
                    in
                        [(a,b) | [a,b] <- tuplesInt]
    where 
        repl    '(' = ' '
        repl    ')' = ' '
        repl    c   = c
        repl'   ',' = ' '
        repl'   c   = c
------------------------------
-- helper sum function -------
------------------------------
mysum :: [Int]->Int
mysum arg = foldr (\acc ad -> acc + ad) 0 arg

-------------------------------
-- makes a Map from given input 
------------------------------
makeMap :: [Int]->[Int]->[(Int,Int)]->Maybe Map 
makeMap rows columns houses | sumc /= sumr                          = Nothing `mydebug` ("1: sumc /= sumr: "++ show sumc ++ "/=" ++ show sumr++" rows: "++show rows++" columns: "++show columns) 
                            | sumr /= counth                        = Nothing `mydebug` "2: sumr /= counth"
                            | (maxcols + 1) > ( length columns )    = Nothing `mydebug` "3: maxcols+1>length cols"
                            | (maxrows + 1) > ( length rows)        = Nothing `mydebug` "4: maxrows+`>length rows"
                            | otherwise                             = Just (rows, columns, matrix )                     
                            
--makeMap :: [Int] -> [Int] -> [(Int,Int)] -> Map
--makeMap rows columns houses = (rows, columns, matrix )
    where   
        sumr    = sum rows
        sumc    = sum columns
        counth  = length houses
        vcols   = [a | (a,b) <- houses]
        vrows   = [b | (a,b) <- houses]
        maxcols = maximum vcols
        maxrows = maximum vrows
        matrix  = makeMatrix (length columns) (length rows) houses


-- helper function (can be improved)
rowToString :: Row->String
rowToString []  = ""
rowToString ((Place _ b) : xs)  | b         = "o" ++ rowToString xs
                                | otherwise = "x" ++ rowToString xs
rowToString (House _ _ : xs)                = "h" ++ rowToString xs
rowToString (Tank _ _ : xs)                 = "t" ++ rowToString xs

-- printing function
printMap :: Map -> IO ()
printMap (rows, columns, vtiles) =  do
                                        putStrLn $ show $ concat (map show columns)
                                        go 0
    where
      go :: Int -> IO ()
      go i  | i < length rows = do
                                    putStrLn $ ((show (rows!!i))++( rowToString (vtiles !! i)))
                                    go (i+1)
            | otherwise   = return ()
-------------------------------
-- make a matrix --------------
-------------------------------
makeMatrix :: Int->Int->[(Int,Int)]->Matrix 
makeMatrix _ 0 _ = []
makeMatrix colno rowno houses = makeMatrix colno (rowno-1) houses ++ [makeRow' (rowno-1) (colno) houses]

-------------------------------
-- makes a row ----------------
-------------------------------
makeRow :: Int->Int->[Int]->Row 
makeRow no size houses  | size == 0 = []
                        | otherwise =
                                ( makeRow no (size-1) houses ) ++ [makeHouse (no,size-1) (isHouse (size-1))]
    where isHouse a= a `elem` houses

makeRow' :: Int->Int->[(Int,Int)]->Row 
makeRow' no size houses  = makeRow no size (housesCols (no))
    where
        housesCols num= [a1 | (b1,a1) <- houses, b1 == num]

-------------------------------
-- makes a tile  --------------
-------------------------------
--for init (all places inited to "can be a Tank")
--           position    is_house result
makeHouse :: (Int,Int) -> Bool -> Tile
makeHouse position house    | house==True   = House position Empty
                            | otherwise     = Place position True
--for further use
--            position  is_house tank can_be_a_tank result
--makeHouse' :: (Int,Int) -> Bool -> Bool -> Bool -> Tile
--makeHouse' position True _ _ = House position 

-------------------------------
-- MAIN -----------------------
-------------------------------
main =  do 
            putStrLn $ "Starting robo_architect..."
            -- pobieranie argumentow
            (file_name:args) <- getArgs

            -- wczytywanie pliku z zadaniem
            handle <- openFile file_name ReadMode
            contents <- hGetContents handle

            -- wczytywanie ze stringa do odpowiednich typów przejsciowych
            let linesOfContents = lines contents
            let rowsStr = linesOfContents !! 0
            let columnsStr = linesOfContents !! 1
            let housesStr = linesOfContents !! 2
            putStrLn $ "rows: "++rowsStr
            putStrLn $ "columns: "++columnsStr
            putStrLn $ "houses: "++housesStr
            let rowsInt = getNums rowsStr
            let columnsInt = getNums columnsStr
            let houses = getTuples housesStr
            putStrLn "======================="
            putStrLn "data loaded to lists:"
            print rowsInt
            print columnsInt
            print houses

            putStrLn "======================="           
            putStrLn "checking data:"
            --sprawdzenie poprawnosci (dlugosc list)
            if  length rowsInt == length columnsInt && length columnsInt == length houses
                then do putStrLn "-bad input file: improper input data sizes"
                        exitFailure
                else putStrLn "+proper input data sizes"
           
            --sprawdzenie poprawnosci (suma domkow i wymagan)
            let rowSum = foldr (\x y -> x+y) 0 rowsInt
            let columnSum = foldr (\x y -> x+y) 0 columnsInt
            if columnSum /= rowSum
                then do putStrLn $ "-bad input file: improper input row/column numbers"
                        exitFailure
                else putStrLn "+proper input row and column numbers equal"
            putStrLn "======================="           
            putStrLn "loading data into proper structures:"
            putStrLn $ "rowsInt: "++(show  rowsInt)
            let firstMap = makeMap (rowsInt) (columnsInt) (houses)
            case firstMap of
                Just somemap-> printMap somemap
                Nothing-> hClose handle
            --print firstMap
            putStrLn "======================="           
            
            putStrLn "algorithm starting..."
            

            --zamkniecie 
            hClose handle
            
--          args <- getArgs
--          putStrLn "The arguments u passed: "
--          mapM_ putStrLn args
--          interact $ unlines . filter ((>10) . length) . lines


--          putStrLn "starting program..."
--          line <- getLine  
--          if null line  
--              then return ()  
--              else do  
--                  putStrLn $ reverseWords line  
--                  main  
--    
--reverseWords :: String -> String  
--reverseWords = unwords . map reverse . words  

--main = getArgs >>= parse >>= putStr . tac
-- 
--tac  = unlines . reverse . lines
-- 
--parse ["-h"] = usage   >> exit
--parse ["-v"] = version >> exit
--parse []     = getContents
--parse fs     = concat `fmap` mapM readFile fs
-- 
--usage   = putStrLn "Usage: tac [-vh] [file ..]"
--version = putStrLn "Haskell tac 0.1"
--exit    = exitWith ExitSuccess
--die     = exitWith (ExitFailure 1)
